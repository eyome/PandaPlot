# -*- coding: utf-8 -*-
"""
Éditeur de Spyder
eyome

Script de dépouillement
"""

#import mdfreader
import pandas as pd
import re
import matplotlib.pyplot as plt
from matplotlib.widgets import MultiCursor


#Importation des donnees
def Importer_fichier(Fichier_a_modifier, separateur):
    if Fichier_a_modifier.endswith(".csv"):
        return pd.read_csv(Fichier_a_modifier, sep = separateur, decimal=",", encoding="ISO-8859-1")
    elif Fichier_a_modifier.endswith(".txt"):
        return pd.read_table(Fichier_a_modifier, sep = separateur, encoding="ISO-8859-1")
    else:
        print ("Fichier non supporte ou extention absente")

    
def Renommer_colonnes(statut):
    #Modification des entetes si besoin
    if statut == "oui":
        les_colonnes = Tableau_a_modifier.columns
        nom_des_colonnes = []
        for nom_colonne in les_colonnes:
            if re.match('CAN[0-9]+::.*::.*::.*::(.*)', nom_colonne):
                nouveau_nom = re.match('CAN[0-9]+::.*::.*::.*::(.*)', nom_colonne).group(1)
                print(nouveau_nom)
            else:
                nouveau_nom = nom_colonne
            nom_des_colonnes.append(nouveau_nom)   
        Tableau_a_modifier.columns = nom_des_colonnes
        print ("Renommage des colonnes")
    elif statut == 'non':
        print ("Pas de renommage des colonnes")


def Tracer_graph(x, y, position, point):
    #Définition des graphiques
    x = Tableau_a_modifier[x]
    y = Tableau_a_modifier[y]
#    plt.yscale(labely)
#    plt.xscale(labelx)
    ax = fig.add_subplot(position)
    ax.plot(x, y, point)
    ax.grid()
    ax.legend(loc = 'best', fontsize = 'small', shadow = 1)
    plt.tight_layout()


#Nom du fichier a charger
Fichier_a_modifier = '16_012975 OLT iBSG 4W A87080 G42383_CheckSum_Version 9.csv'

Tableau_a_modifier = Importer_fichier(Fichier_a_modifier, separateur = ';')

Renommer_colonnes('non')

#Graphiques à tracer :   
fig = plt.figure(facecolor='white')
Tracer_graph(x = 'Time[s]', y = 'MASTER_CTRL_REQ_Checksum', position = 311, point= '-')
Tracer_graph('Time[s]', 'MASTER_TrqCtrl_Pos_Trq_Lim[Nm]', 312, 'g-')
Tracer_graph('Time[s]', 'MASTER_EXT_REQ_Checksum', 313, 'r-')


#Multicurseur sur tous les graphs
multi = MultiCursor(fig.canvas, tuple(fig.axes), lw=1,
                    horizOn=True, vertOn=True)


fig.show

#character	description
#'-'	solid line style
#'--'	dashed line style
#'-.'	dash-dot line style
#':'	dotted line style
#'.'	point marker
#','	pixel marker
#'o'	circle marker
#'v'	triangle_down marker
#'^'	triangle_up marker
#'<'	triangle_left marker
#'>'	triangle_right marker
#'1'	tri_down marker
#'2'	tri_up marker
#'3'	tri_left marker
#'4'	tri_right marker
#'s'	square marker
#'p'	pentagon marker
#'*'	star marker
#'h'	hexagon1 marker
#'H'	hexagon2 marker
#'+'	plus marker
#'x'	x marker
#'D'	diamond marker
#'d'	thin_diamond marker
#'|'	vline marker
#'_'	hline marker

#character	color
#‘b’	blue
#‘g’	green
#‘r’	red
#‘c’	cyan
#‘m’	magenta
#‘y’	yellow
#‘k’	black
#‘w’	white